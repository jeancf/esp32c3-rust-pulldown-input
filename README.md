# Minimalistic test case for problem with the configuration of GPIO as input pin with weak pull-down resistor

I want to configure an input pin on an esp32c3 with a weak pull-down resistor so that when the pin is floating the input signal is registered as *low*.

I tested GPIO8 with 2 different methods (using HAL and manipulating registers) and with 2 different boards (AI thinker and Lilygo) with the same result: **the pin is read *high* when it is not connected.

I expect it to be *low* when unconnected or connected to ground and *high* only when it is connected to VCC.

## With HAL

```rust
let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
let mut echo = io.pins.gpio8.into_pull_down_input();
echo.enable_input(true);
```

Then I check the status of the pin with `echo.is_high().unwrap()` and `echo.is_low().unwrap()`.

## With register manipulation

```rust
peripherals.IO_MUX.gpio[8].write(|w| {
    w.fun_wpd().set_bit()
     .fun_wpu().clear_bit()
     .fun_ie().set_bit()
    });
```

Then I check the status of the pin with `peripherals.GPIO.in_.read().data_next().bits() & (0b1 << 8)) >> 8 == 1`

**What am I missing? Do I need to configure something else?**

## UPDATE

**If I run the same test case with GPIO0 instead of GPIO8, the behavior is correct**

So I checked all pins in a [separate branch of the repo](https://gitlab.com/jeancf/esp32c3-rust-pulldown-input/-/tree/all_pins) and found out that pins 0-7 behave as expected but a number of others don´t (e.g. 8, 9, 10).
