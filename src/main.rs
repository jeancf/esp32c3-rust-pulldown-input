#![no_std]
#![no_main]

use esp32c3_hal::{
    clock::ClockControl, pac::Peripherals, prelude::*, timer::TimerGroup, IO, Delay, RtcCntl, gpio_types::InputPin,
};
use esp_backtrace as _;
use esp_println::*;
use riscv_rt::entry;

#[entry]
fn main() -> ! {

    println!("starting esp32c3-rust-pulldown-input");

    let peripherals = Peripherals::take().unwrap();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    // Disable the RTC and TIMG watchdog timers
    let mut rtc_cntl = RtcCntl::new(peripherals.RTC_CNTL);
    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    let mut wdt0 = timer_group0.wdt;
    let timer_group1 = TimerGroup::new(peripherals.TIMG1, &clocks);
    let mut wdt1 = timer_group1.wdt;

    rtc_cntl.set_super_wdt_enable(false);
    rtc_cntl.set_wdt_global_enable(false);
    wdt0.disable();
    wdt1.disable();

    let mut delay = Delay::new(&clocks);
    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut echo = io.pins.gpio8.into_pull_down_input();
    echo.enable_input(true);

    loop {
        if echo.is_high().unwrap() {
            println!("echo is high");    
        } else if echo.is_low().unwrap() {
            println!("echo is low");
        } else {
            println!("echo is undefined");
        }
        delay.delay_ms(1000u32);
    }
}
